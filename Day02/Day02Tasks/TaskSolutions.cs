﻿
namespace Day02Tasks
{
    public class TaskSolutions
    {
        private static int[] fibonacciSequence = new int[50];
        public static bool HasFibonocciOrder(int[][] matrix)
        {
            InitializeFibunacciSequence();

            for (int x = 2; x <= 4; x++)
            {
                for (int j = 0; j < matrix.GetLength(0) - x; j++)
                {
                    for (int i = 0; i < matrix.GetLength(0) - x; i++)
                    {
                        if (CheckAscending(i, j, matrix, x) && CheckDescending(i + x, j + x, matrix, x))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private static void InitializeFibunacciSequence()
        {
            fibonacciSequence[0] = 0;
            fibonacciSequence[1] = 1;

            for (int i = 2; i < fibonacciSequence.Length; i++)
            {
                fibonacciSequence[i] = fibonacciSequence[i - 2] + fibonacciSequence[i - 1];
            }
        }

        private static bool CheckAscending(int i, int j, int[][] matrix, int sqrLength)
        {
            int neededIndex = 0;
            bool haveOne = false;
            for (int x = 0; x < sqrLength; x++)
            {
                for (int y = 0; y < sqrLength; y++)
                {
                    if (!fibonacciSequence.Contains(matrix[x][y]))
                    {

                        return false;
                    }
                    int fibunacciNum = matrix[x][y];
                    if (haveOne)
                    {
                        if (fibonacciSequence[neededIndex + 1] == fibunacciNum)
                        {
                            neededIndex++;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        for (int a = 0; a < fibonacciSequence.Length; a++)
                        {

                            if (fibonacciSequence[a] == fibunacciNum)
                            {
                                neededIndex = a;
                                haveOne = true;
                                break;
                            }
                        }
                    }
                }
            }
            return true;
        }
        private static bool CheckDescending(int i, int j, int[][] matrix, int sqrLength)
        {
            int placeInSequence = -1;
            for (int k = 0; k < fibonacciSequence.Length; k++)
            {
                if (matrix[j][i] == fibonacciSequence[k])
                {
                    placeInSequence = k;
                    break;
                }
            }
            if (placeInSequence == -1)
            {
                return false;
            }

            int lastIndex = Math.Abs(i - sqrLength) + 1;
            for (int x = j; x >= lastIndex; x--)
            {
                if (x == j)
                {
                    for (int y = i - 1; y >= lastIndex; y--)
                    {
                        if (matrix[x][y] != fibonacciSequence[++placeInSequence])
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    if (matrix[x][lastIndex] != fibonacciSequence[++placeInSequence])
                    {
                        return false;
                    }
                }
            }

            return true; ;
        }
    }
}
